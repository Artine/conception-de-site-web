-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 28 nov. 2019 à 23:49
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `projet_esig`
--

-- --------------------------------------------------------

--
-- Structure de la table `election`
--

DROP TABLE IF EXISTS `election`;
CREATE TABLE IF NOT EXISTS `election` (
  `id_election` int(10) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `archive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_election`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `election`
--

INSERT INTO `election` (`id_election`, `nom`, `archive`) VALUES
(2, 'election_2019', 1),
(3, 'election_2017', 1),
(4, 'election_2018', 1),
(9, 'election 2020', 0);

-- --------------------------------------------------------

--
-- Structure de la table `sujet`
--

DROP TABLE IF EXISTS `sujet`;
CREATE TABLE IF NOT EXISTS `sujet` (
  `id_sujet` int(10) NOT NULL AUTO_INCREMENT,
  `id_election` int(10) NOT NULL,
  `titre` varchar(100) NOT NULL,
  `url_poster` varchar(200) NOT NULL,
  `nbre_vote_final` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_sujet`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `sujet`
--

INSERT INTO `sujet` (`id_sujet`, `id_election`, `titre`, `url_poster`, `nbre_vote_final`) VALUES
(1, 2, 'nom1', '1.jpg', 0),
(2, 2, 'nom2', '2.jpg', 0),
(3, 2, 'nom3', '3.jpg', 0),
(4, 2, 'nom4', '4.jpg', 0),
(5, 3, 'nom1bis', '1.jpg', 10),
(6, 2, 'nom6', '5.jpg', 0),
(7, 4, 'nom1bis', '1.jpg', 25),
(8, 3, 'nom2bis', '2.jpg', 50),
(9, 4, 'nom4bis', '4.jpg', 2),
(25, 2, 'ping6', '25.jpg', 2),
(26, 6, 'ping test', '26.jpg', 0),
(27, 2, 'ping1', '27.jpg', 0),
(24, 2, 'ping12', '24.jpg', 0),
(23, 2, 'ping11', '23.jpg', 0),
(22, 2, 'ping1000000000000000000000000000000', '22.jpg', 1),
(28, 8, 'ping100', '28.jpg', 0);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `motdepasse` varchar(100) NOT NULL,
  `mail` varchar(100) NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `id_sujet_vote` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=126 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `login`, `motdepasse`, `mail`, `admin`, `id_sujet_vote`) VALUES
(125, 'etudiant2', '$2y$10$uFvN9xLXK46eE68GpwweDeylFXdXB.j9aE3/R.dVdWMTyGpOLmO9m', 'samuellgdu29217@hotmail.fr', 0, NULL),
(122, 'etudiant', '$2y$10$Wlb9aGoEAw.oELWZ/mmPJ.A0kjCkISpxjz7KELQLPNyAA4ID6kK4q', 'samuellgdu29217@hotmail.fr', 0, NULL),
(118, 'admin2', '$2y$10$.KhspBlXF.ag68YspSWknupe20ByT6k1uNSCDUGmQws71gopcEXrO', 'samuellgdu29217@hotmail.fr', 1, NULL),
(117, 'admin', '$2y$10$.zvRzIiST7.r6/ojHGt2FutI9N2eGO4Nx..FDAW0UkMaFXZ8cnw0i', 'samuellgdu29217@hotmail.fr', 1, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
