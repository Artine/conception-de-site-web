<?php
//On alimente la base de données
include("param.inc.php"); 

if (isset ($_POST['deconnecter'])){
//detruit toute variable session
  $_SESSION = array();

//puis detruit la session
  session_destroy();

  header('Location: index.php');
}


//On se connecte
$conn = new mysqli($servername, $username, $password, $dbname); 

if ($conn->connect_errno) {
 echo "Echec lors de la connexion à MySQL : (" . $conn->
 connect_errno . ") " . $conn->connect_error;
}
else{
//lance requete creation user
  if (isset ($_POST['inscrire'])){




  //On récupère les valeurs entrées par l'utilisateur :
    $nom=$conn->real_escape_string(htmlspecialchars($_POST['identifiant']));
    $mail=$conn->real_escape_string(htmlspecialchars($_POST['mail']));
    $mdp=$conn->real_escape_string(htmlspecialchars($_POST['mdp']));

   /*
https://stackoverflow.com/questions/28936141/using-password-hash-and-password-verify
   */

   // si les saisies ne sont pas vides
if( (!empty($nom)) && (!empty($mail)) && (!empty($mdp))){ 


  $mdp = password_hash($mdp, PASSWORD_BCRYPT);

  //on verifie si le pseudo est disponible, ou si il existe deja dans la bdd
  if ($stmt = $conn->prepare("SELECT COUNT(*) FROM utilisateur WHERE login=?")) {

    // Bind a variable to the parameter as a string. 
    $stmt->bind_param("s", $nom);

    // Execute the statement.
    $stmt->execute();
    
    // Get the variables from the query.
    $stmt->bind_result($res);

    // Fetch the data.
    $stmt->fetch();

    // Close the prepared statement.
    $stmt->close();

    
    if ($res== 0) {//donc pas de login identique dans bdd

      //On prépare la commande sql d'insertion
      $sql = "INSERT INTO utilisateur (login,motdepasse,mail) VALUES (?,?,?)"; 

      if (!($stmt = $conn->prepare($sql))) {
       echo "Echec de la préparation : (" . $conn->errno . ") " . $conn->
       error;
     }
 // s pour chaine ; i pour entier ; d pour nombre decimal
     $stmt->bind_param("sss" ,$nom,$mdp,$mail);

     if (!$stmt->execute()) {
       echo "Echec lors de l’exécution de la requête : (" . $stmt->
       errno . ") " . $stmt->error;
     }
     $_SESSION['erreur_inscr']="";
     header('Location: connexion_page.php');

     $stmt->close();

   }else{
    $_SESSION['erreur_inscr']="er_loginpris";
  }

}

}else{
  $_SESSION['erreur_inscr']="er_champs";
}

}

if (isset ($_POST['nom_election_ok'])){

  //On récupère les valeurs entrées par l'utilisateur :
  $nom=$conn->real_escape_string(htmlspecialchars($_POST['nom_election']));
  
   /*
https://stackoverflow.com/questions/28936141/using-password-hash-and-password-verify
   */

   // si les saisies ne sont pas vides
if( (!empty($nom))){ 

      //On prépare la commande sql d'insertion
  $sql = "INSERT INTO election (nom) VALUES (?)"; 

  if (!($stmt = $conn->prepare($sql))) {
   echo "Echec de la préparation : (" . $conn->errno . ") " . $conn->
   error;
 }
 // s pour chaine ; i pour entier ; d pour nombre decimal
 $stmt->bind_param("s" ,$nom);

 if (!$stmt->execute()) {
   echo "Echec lors de l’exécution de la requête : (" . $stmt->
   errno . ") " . $stmt->error;
 }
 $_SESSION['erreur_crea_election']="";
 header('Location: menu_election.php');

 $stmt->close();



}else{
  $_SESSION['erreur_crea_election']="er_champs";
}

}

if (isset ($_POST['connecter'])){




  //On récupère les valeurs entrées par l'utilisateur :
  $nom=$conn->real_escape_string(htmlspecialchars($_POST['identifiant']));
  $mdp=$conn->real_escape_string(htmlspecialchars($_POST['mdp']));
   /*
https://stackoverflow.com/questions/28936141/using-password-hash-and-password-verify
   */

   // si les saisies ne sont pas vides
if( (!empty($nom)) && (!empty($mdp))){ 

  //On prépare la commande sql d'insertion

  if ($stmt = $conn->prepare("SELECT motdepasse,admin FROM utilisateur WHERE login=?")) {

    // Bind a variable to the parameter as a string. 
    $stmt->bind_param("s", $nom);

    // Execute the statement.
    $stmt->execute();
    
    // Get the variables from the query.
    $stmt->bind_result($hash,$admin);

    // Fetch the data.
    $stmt->fetch();

    //quoi qu'il arrive, il recupère un mot de passe hash tant que le login existe, et rien si login pas dans bdd

    $auth = password_verify($mdp, $hash);

    // Close the prepared statement.
    $stmt->close();

    if (!empty($hash)) {// si $hash n'est pas vide, login existe
    if ($auth ==true) {
      /*echo 'connection ok';*/
      $_SESSION['erreur']="";
      $_SESSION['connection']= true;
      $_SESSION['login']= $nom;
      $_SESSION['connection']= true;
      if($admin == 1){
        $_SESSION['admin']= true;
      }else{
        $_SESSION['admin']= false;
      }
      header('Location: index_connected.php');
    }else{
      $_SESSION['erreur']="er_mdp";
    }


  }else{
    $_SESSION['erreur']="er_login";
  }


}

}else{
  $_SESSION['erreur']="er_champs";
}

}

if (isset ($_POST['fermeture'])){
  
  $sql = "SELECT id_election FROM election WHERE archive= 0 ORDER BY id_election DESC";

  $result = $conn->query($sql);


$row = $result->fetch_assoc();// on ne recupère que le premier resultat qui correspond a la dernière election crée et non archivée.
$id_election = $row["id_election"];

/* Libération des résultats */
$result->free();



  $sql = 'UPDATE election SET `archive`= 1 WHERE `id_election` = \''.$id_election.'\' ';

  $res=$conn->query($sql);

  $sql = "SELECT `id_sujet_vote`,COUNT(*) AS nbre_votes_final FROM utilisateur GROUP BY id_sujet_vote ORDER BY id_sujet_vote DESC";

  $result = $conn->query($sql);

  $num_lignes=$result->num_rows;

  $liste_id_vote = array();
  $liste_nbre_vote = array();
  //Tableau associatif 
while ($row = $result->fetch_assoc()) {
  if($row["id_sujet_vote"] != NULL){
    array_push($liste_id_vote, $row["id_sujet_vote"]);
    array_push($liste_nbre_vote, $row["nbre_votes_final"]);
  }
  
}

for($i=0;$i< count($liste_id_vote) ;$i++){
  $sql = 'UPDATE sujet SET `nbre_vote_final`= '.$liste_nbre_vote[$i].' WHERE `id_sujet` = \''.$liste_id_vote[$i].'\' ';

  $res=$conn->query($sql);

}
$sql = 'UPDATE utilisateur SET `id_sujet_vote`= NULL';

  $res=$conn->query($sql);

$_SESSION['gestion'] = "fin_elec";

}


}//fin du else quand connection marche


mysqli_close($conn);



function get_nom_election(){

//On alimente la base de données
  include("param.inc.php");
//On se connecte
  $conn = new mysqli($servername, $username, $password, $dbname); 



  /* Vérification de la connexion */
  if ($conn->connect_errno) {
    printf("Echec de la connexion : %s\n", $conn->connect_error);
    exit();
  }

  $sql = "SELECT nom,id_election FROM election WHERE archive= 0 ORDER BY id_election DESC";

  $result = $conn->query($sql);


$row = $result->fetch_assoc();// on ne recupère que le premier resultat qui correspond a la dernière election crée et non archivée.
$nom_election = $row["nom"];
$id_election = $row["id_election"];

/* Libération des résultats */
$result->free();

//maintenant on prepare une requete qui recupère tous les sujets correspondant à cette election
$sql = 'SELECT * FROM sujet WHERE id_election="'.$id_election.'" ORDER BY id_sujet ASC';

$result = $conn->query($sql);

//compte le nombre de ligne de resultat
$num_lignes=$result->num_rows;
//printf("Select a retourné %d lignes.\n", $result->num_rows);

$liste_id = array();
$liste_nom = array();
$liste_url = array();

//Tableau associatif 
while ($row = $result->fetch_assoc()) {
        //printf ("%s %s \n", $row["titre"],$row["url_poster"]);
  array_push($liste_id, $row["id_sujet"]);
  array_push($liste_nom, $row["titre"]);
  array_push($liste_url, $row["url_poster"]);

}

/* Libération des résultats */
$result->free();

$name= $_SESSION['login'];

$sql = 'SELECT id_sujet_vote FROM utilisateur WHERE login="'.$name.'"';

$result = $conn->query($sql);

//printf("Select a retourné %d lignes.\n", $result->num_rows);

$row = $result->fetch_assoc();

$id_sujet_vote = $row["id_sujet_vote"];



return array($num_lignes,$nom_election,$liste_id,$liste_nom,$liste_url,$id_sujet_vote);


/* Libération des résultats */
$result->free();

/* Fermeture de la connexion */
$conn->close();

}

function setvote(){

//On alimente la base de données
  include("param.inc.php");
//On se connecte
  $conn = new mysqli($servername, $username, $password, $dbname); 



  /* Vérification de la connexion */
  if ($conn->connect_errno) {
    printf("Echec de la connexion : %s\n", $conn->connect_error);
    exit();
  }
  $num_vote=$_SESSION['vote'];
  $name= $_SESSION['login'];

  $sql = 'UPDATE utilisateur SET `id_sujet_vote`= '.$num_vote.' WHERE `login` = \''.$name.'\' ';

  $res=$conn->query($sql);

  /* Fermeture de la connexion */
  $conn->close();

}

function gestion(){

//On alimente la base de données
  include("param.inc.php");
//On se connecte
  $conn = new mysqli($servername, $username, $password, $dbname); 



  /* Vérification de la connexion */
  if ($conn->connect_errno) {
    printf("Echec de la connexion : %s\n", $conn->connect_error);
    exit();
  }

  $sql = 'SELECT COUNT(*) AS compte FROM election WHERE archive= 0 ';

  $res=$conn->query($sql);

  $row = $res->fetch_assoc();
  if($row["compte"] == 0){
    $_SESSION['gestion']="0_elec_encours";
  }else{
    $_SESSION['gestion']="elec_encours";
  }

  /* Fermeture de la connexion */
  $conn->close();

}

function get_liste_election_closed(){

//On alimente la base de données
  include("param.inc.php");
//On se connecte
  $conn = new mysqli($servername, $username, $password, $dbname); 



  /* Vérification de la connexion */
  if ($conn->connect_errno) {
    printf("Echec de la connexion : %s\n", $conn->connect_error);
    exit();
  }

  $sql = "SELECT nom,id_election FROM election WHERE archive= 1 ORDER BY id_election DESC";

  $res=$conn->query($sql);

  $liste_id = array();
  $liste_nom = array();

  $num_lignes=$res->num_rows;

//Tableau associatif 
  while ($row = $res->fetch_assoc()) {
        //printf ("%s %s \n", $row["titre"],$row["url_poster"]);
    array_push($liste_id, $row["id_election"]);
    array_push($liste_nom, $row["nom"]);

  }
  return array($num_lignes,$liste_id,$liste_nom);
  /* Fermeture de la connexion */
  $conn->close();

}

function get_classement_election($id_election){

//On alimente la base de données
  include("param.inc.php");
//On se connecte
  $conn = new mysqli($servername, $username, $password, $dbname); 



  /* Vérification de la connexion */
  if ($conn->connect_errno) {
    printf("Echec de la connexion : %s\n", $conn->connect_error);
    exit();
  }

  $sql = 'SELECT nom FROM election WHERE id_election="'.$id_election.'"';

  $result = $conn->query($sql);


$row = $result->fetch_assoc();// on ne recupère que le premier resultat qui correspond a la dernière election crée et non archivée.
$nom_election = $row["nom"];

/* Libération des résultats */
$result->free();

//maintenant on prepare une requete qui recupère tous les sujets correspondant à cette election
$sql = 'SELECT * FROM sujet WHERE id_election="'.$id_election.' " ORDER BY nbre_vote_final DESC ';

$result = $conn->query($sql);

//compte le nombre de ligne de resultat
$num_lignes=$result->num_rows;
//printf("Select a retourné %d lignes.\n", $result->num_rows);

$liste_id = array();
$liste_nom = array();
$liste_url = array();
$liste_nbre_vote_final = array();

//Tableau associatif 
while ($row = $result->fetch_assoc()) {
        //printf ("%s %s \n", $row["titre"],$row["url_poster"]);
  array_push($liste_id, $row["id_sujet"]);
  array_push($liste_nom, $row["titre"]);
  array_push($liste_url, $row["url_poster"]);
  array_push($liste_nbre_vote_final, $row["nbre_vote_final"]);
}

/* Libération des résultats */
$result->free();


return array($num_lignes,$nom_election,$liste_id,$liste_nom,$liste_url,$liste_nbre_vote_final);


/* Libération des résultats */
$result->free();

/* Fermeture de la connexion */
$conn->close();

}
echo'';


?>