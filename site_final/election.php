<?php
session_start();
	include('recup.php');
	 if (!isset($_SESSION['connection'])) {
	header ('Location: index.php');
	exit();
}
if (!isset($_GET['num_election'])) {
	header ('Location: index_connected.php');
}

?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<title>Site projet</title>
	<link rel="stylesheet" href="html/bootstrap-4.3.1/css/bootstrap.css" />
	<link rel="stylesheet" href="html/fontawesome-5.11.2/css/all.css">
	<link rel="stylesheet" href="html/css/style.css">

	<script src="html/js/jquery-2.1.4.min.js"></script>
	<script src="html/js/code_page.js" ></script>

</head>
<body>
<header class="main_header">
	<a href="index_connected.php"><img src="html/img/logo.jpg" alt="logo esigelec"></a>
	<h1>Projet Ping</h1>
	
	<div class="groupement_btns">
		
	<form method="post" action="index.php">
		<input name="deconnecter" type="submit" class="btn btn-primary" value="Se déconnecter" />
	</form>
	</div>


</header>

<nav>
<ul class="main_nav">
		<li class="nav-item">
			<a class="nav-link home " href="index_connected.php">
				<span class="fa fa-home" aria-hidden="true"></span>
			</a>
		</li>
		<li class="nav-item ">
			<a class="nav-link " href="liste_election.php">Election(s) terminée(s)</a>
		</li>
		
		
		<?php
	if($_SESSION['admin']==true){
		echo '<li class="nav-item ">
			<a class="nav-link" href="menu_election.php">Menu de gestion des élections</a>
		</li>';

	}

	?>
</ul>
</nav>

<div class="contenu">
	
	<?php 
			$tab = get_classement_election($_GET['num_election']);
			$num_rows=$tab[0];
			$name_election=$tab[1];
			$liste_id=$tab[2];
			$liste_nom=$tab[3];
			$liste_url=$tab[4];
			$nbre_vote=$tab[5];
			
			echo '<h1>'.$name_election.'</h1>
			<div class="center" >
				<h2>Classement des sujets PING participants :</h2>
			</div>
			<hr>';

			
	for($i=0;$i<$num_rows;$i++){
			echo '<div class="notif">
			<div class="image"><img src="html/img/'.$liste_url[$i].'" alt="img_sujet"></div>
			<div class="nom"> Sujet n°'.($i+1).' : '.$liste_nom[$i].' ( '.$nbre_vote[$i].' <i class="fas fa-fire"></i> ) </div></div>';
		}
	if($num_rows == 0){
		echo '<div class="center"><div class=" alert alert-danger" role="alert">
				<span class="fa fa-exclamation-triangle" aria-hidden="true"></span>
				<span class="apres_gly">Cette élection n\'a aucun sujets d\'ajouter</span>
			</div></div>'; 
	}

	?>


</div>
<footer>
	<span>Un site de Samuel LE GALL et Artine ADIKPETO</span>
</footer>
</body>
</html>
