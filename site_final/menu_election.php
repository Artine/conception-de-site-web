<?php
session_start();
include('recup.php');
if (!isset($_SESSION['connection'])) {
	header ('Location: index.php');
	exit();
}
if ($_SESSION['admin'] == false) {
	header ('Location: index_connected.php');
	exit();
}
if (isset($_SESSION['erreur_upload'])) {
	$_SESSION['erreur_upload']="";
}
gestion();

?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<title>Site projet</title>
	<link rel="stylesheet" href="html/bootstrap-4.3.1/css/bootstrap.css" />
	<link rel="stylesheet" href="html/fontawesome-5.11.2/css/all.css">
	<link rel="stylesheet" href="html/css/style.css">

	<script src="html/js/jquery-2.1.4.min.js"></script>
	<script src="html/js/code_page.js" ></script>

</head>
<body>
	<header class="main_header">
		<a href="index_connected.php"><img src="html/img/logo.jpg" alt="logo esigelec"></a>
		<h1>Projet Ping</h1>

		<div class="groupement_btns">

			<form method="post" action="index.php">
				<input name="deconnecter" type="submit" class="btn btn-primary" value="Se déconnecter" />
			</form>
		</div>


	</header>

	<nav>
		<ul class="main_nav">
			<li class="nav-item">
				<a class="nav-link home " href="index_connected.php">
					<span class="fa fa-home" aria-hidden="true"></span>
				</a>
			</li>
			<li class="nav-item ">
				<a class="nav-link " href="liste_election.php">Election(s) terminée(s)</a>
			</li>


			<?php
			if($_SESSION['admin']==true){
				echo '<li class="nav-item ">
				<a class="nav-link active" href="#nogo">Menu de gestion des élections</a>
				</li>';

			}

			?>
		</ul>
	</nav>

	<div class="contenu_connected">
		<form action="menu_election.php" method="post" enctype="multipart/form-data">
		<div class="contener_centre">
			<?php
				if($_SESSION['gestion'] == "0_elec_encours"){
					echo '<a class="menu btn btn-primary" href="Creation.php" id="creation" role="button" >Créer une élection</a>

<a class="menu btn btn-primary disabled" href="ajouter_sujet.php" id= "creer_sujet" role="button">Ajouter un sujet à l\'élection en cours</a>

<input type="submit" class="menu btn btn-primary"  name= "fermeture"  value="Clôturer l`\'élection en cours" disabled>

					';
				}elseif($_SESSION['gestion'] == "elec_encours"){
				echo '<a class="menu btn btn-primary disabled" href="Creation.php" id="creation" role="button" >Créer une élection</a>

<a class="menu btn btn-primary" href="ajouter_sujet.php" id= "creer_sujet" role="button" >Ajouter un sujet à l\'élection en cours</a>

<input type="submit" class="menu btn btn-primary"  name= "fermeture"  value="Clôturer l`\'élection en cours">';
			}
			
			?>

		</div> 
	</form>
	</div>

	<footer>
		<span>Un site de Samuel LE GALL et Artine ADIKPETO</span>
	</footer>
</body>
</html>
