<?php
include("param.inc.php"); 

// on se connecte a la bdd
$conn = new mysqli($servername, $username, $password, $dbname); 

if ($conn->connect_errno) {
   echo "Echec lors de la connexion à MySQL : (" . $conn->
   connect_errno . ") " . $conn->connect_error;
}
else{

// Vérifier si le formulaire a été soumis
    if($_SERVER["REQUEST_METHOD"] == "POST"){


            //On récupère les valeurs entrées par l'utilisateur :
            $nom=$conn->real_escape_string(htmlspecialchars($_POST['titre_sujet']));



    // Vérifie si le fichier a été uploadé sans erreur.
        if( (isset($_FILES["photo"]) && $_FILES["photo"]["error"] == 0) && (!empty($nom)) ){

            $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
            $filename = $_FILES["photo"]["name"];
            $filetype = $_FILES["photo"]["type"];
            $filesize = $_FILES["photo"]["size"];

        // Vérifie l'extension du fichier
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if(!array_key_exists($ext, $allowed)) die("Erreur : Veuillez sélectionner un format de fichier valide.");

        // Vérifie la taille du fichier - 2Mo maximum
            $maxsize = 2 * 1024 * 1024;
            if($filesize > $maxsize) die("Error: La taille du fichier est supérieure à la limite autorisée.");

        // Vérifie le type MIME du fichier
            if(in_array($filetype, $allowed)){

            //on recupere l'id le plus elevé dans la table sujet de la bdd
                $sql = "SELECT id_sujet FROM sujet ORDER BY id_sujet DESC";

                $result = $conn->query($sql);


            $row = $result->fetch_assoc();// on ne recupère que le premier resultat qui correspond a la dernière election crée et non archivée.
            $id_sujet = $row["id_sujet"];

            /* Libération des résultats */
            $result->free();

            //on affecte à la variable $id_fichier la valeur id+1
            $id_sujet +=1 ;


            // On renomme le fichier
            $filename = $id_sujet .'.'. $ext;
            
            //on upload le fichier sur le site
            move_uploaded_file($_FILES["photo"]["tmp_name"], "html/img/".$filename);

           


            //on recupère l'id de l'élection non archivé
            $sql = "SELECT id_election FROM election WHERE archive= 0 ORDER BY id_election DESC";

            $result = $conn->query($sql);


            $row = $result->fetch_assoc();// on ne recupère que le premier resultat qui correspond a la dernière election crée et non archivée.
            $id_election = $row["id_election"];

            //Libération des résultats 
            $result->free();


            //on prepare la requete d'insersion
            $sql = "INSERT INTO sujet (id_election,titre,url_poster) VALUES (?,?,?)"; 

            if (!($stmt = $conn->prepare($sql))) {
             echo "Echec de la préparation : (" . $conn->errno . ") " . $conn->
             error;
         }
 // s pour chaine ; i pour entier ; d pour nombre decimal
         $stmt->bind_param("sss" ,$id_election,$nom,$filename);

         if (!$stmt->execute()) {
             echo "Echec lors de l’exécution de la requête : (" . $stmt->
             errno . ") " . $stmt->error;
         }

         $stmt->close();
          //on indique que l'upload a eut lieu
          
            $_SESSION['erreur_upload']='ok';



     } else{
        $_SESSION['erreur_upload']='er_tel';
    }
} else{
        //echo "Error: " . $_FILES["photo"]["error"];
    $_SESSION['erreur_upload']='er_inc';
}


}
}
?>