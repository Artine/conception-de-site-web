<?php
session_start();
	include('recup.php');
	// innitialise les variable session
	$_SESSION['erreur_inscr']="";
	if (isset($_SESSION['connection'])) {
	header ('Location: index_connected.php');
	exit();
}
		
?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<title>Site projet</title>
	<link rel="stylesheet" href="html/bootstrap-4.3.1/css/bootstrap.css" />
	<link rel="stylesheet" href="html/fontawesome-5.11.2/css/all.css">
	<link rel="stylesheet" href="html/css/style.css">

	<script src="html/js/jquery-2.1.4.min.js"></script>
	<script src="html/js/code_page.js" ></script>
</head>
<body>
<header class="main_header">
	<a href="index.php"><img src="html/img/logo.jpg" alt="logo esigelec"></a>
	<h1>Projet Ping</h1>
	
	<div class="groupement_btns">
	</div>


</header>
<!--
<nav class="main_nav">
	futur navbar
</nav>
-->
<div class="contenu">
	<h3 class="center">Formulaire de Connexion</h3>
	<br>
	<?php 
		if ($_SESSION['erreur'] == "er_mdp"){
			echo '<div class="center"><div class=" alert alert-danger" role="alert">
				<span class="fa fa-exclamation-triangle" aria-hidden="true"></span>
				<span class="apres_gly"> Mot de passe incorrect. </span>
			</div></div>'; 
		}else if ($_SESSION['erreur'] == "er_login"){
			echo '<div class="center"><div class=" alert alert-danger" role="alert">
				<span class="fa fa-exclamation-triangle" aria-hidden="true"></span>
				<span class="apres_gly"> login incorrect. </span>
			</div></div>';
		}else  if ($_SESSION['erreur'] == "er_champs"){
			echo '<div class="center"><div class=" alert alert-danger" role="alert">
				<span class="fa fa-exclamation-triangle" aria-hidden="true"></span>
				<span class="apres_gly"> Veuillez remplir tous les champs. </span>
			</div></div>';
		}
		else{
		}  
	?>
	
<form name="inscription" method="post" action="connexion_page.php">
	<div class="center">
	<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-user"></i></span>
						</div>
						<input name="identifiant" type="text" class="form-control" id="pseudo"  placeholder="taper votre identifiant">
						
					</div>
</div>
<div class="center">
  <div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-key"></i></span>
						</div>
						<input name="mdp" type="password" class="form-control" id="motdepasse" placeholder="entrer votre mot de passe">
					</div>
	</div>
  
  <div class="center">
  	<input name="connecter" type="submit" class="btn btn-primary" value="Se Connecter" />
  </div>


<br>
<a href="inscription_page.php">Vous n'êtes pas encore inscrit ?</a>


</form>





</div>
<footer>
	<span>Un site de Samuel LE GALL et Artine ADIKPETO</span>
</footer>
</body>
</html>