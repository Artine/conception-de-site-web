<?php
session_start();
	include('recup.php');
	$_SESSION['erreur']="";
	$_SESSION['erreur_inscr']="";
	if (isset($_SESSION['connection'])) {
	header ('Location: index_connected.php');
	exit();
}

?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<title>Site projet</title>
	<link rel="stylesheet" href="html/bootstrap-4.3.1/css/bootstrap.css" />
	<link rel="stylesheet" href="html/fontawesome-5.11.2/css/all.css">
	<link rel="stylesheet" href="html/css/style.css">

	<script src="html/js/jquery-2.1.4.min.js"></script>
	<script src="html/bootstrap-4.3.1/js/bootstrap.min.js"></script>
	<script src="html/js/code_page.js" ></script>

</head>
<body>
<header class="main_header">
	<a href="#nogo"><img src="html/img/logo.jpg" alt="logo esigelec"></a>
	<h1>Projet Ping</h1>
	
	<div class="groupement_btns">
		
	<a href="inscription_page.php" class="btn btn-primary">
		S'inscrire
	</a>
		
	<a href="connexion_page.php" class="btn btn-primary">
		Se Connecter
	</a>
	
	</div>


</header>
<!--
<nav class="main_nav">
	futur navbar
</nav>
-->
<div class="contenu">
	<h1>Le PING ? Qu'est-ce que c'est ?</h1>
	<hr>
	<div class="accueil">
		<div class="text_accueil">
			<p>Le Projet ingénieur, aussi appelé PING se déroule en 3ème année du cycle ingénieur. A l'ESIGELEC, il se réalise durant le semestre 9, par équipes de 6. Il permet de réaliser un projet dans des conditions
quasi industrielles, en relation avec un commanditaire (en majorité une entreprise). </p>
<p>Le projet souvent multidisciplinaire, regroupe des étudiants de différentes dominantes, comme de différentes voies d’accès (cursus classique, apprentissage, cursus formation continue Fontanet).</p><p>Chaque équipe a pour mission de réaliser une étude de faisabilité technico économique validée par la structure donneuse d’ordre, d'élaborer le cahier des charges, de réaliser le projet et d'exposer publiquement le résultat de ses travaux.</p>

		</div>
	
		<div class="img_accueil">
			<img  src="html/img/home.jpg" alt="img_accueil">
		</div>
	</div>
		<div class="text_accueil2">
			
		</div>

		<hr>
	<h4>Exemples de projets PING</h4>
	<div class="center">
		<div id="demo" class="carousel slide" data-ride="carousel" >

    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#demo" data-slide-to="0" class="active"></li>
      <li data-target="#demo" data-slide-to="1"></li>
      <li data-target="#demo" data-slide-to="2"></li>
      <li data-target="#demo" data-slide-to="3"></li>
    </ul>

    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="html/img/ex1.jpg" alt="img_ping">
      </div>
      <div class="carousel-item">
        <img src="html/img/ex2.jpg" alt="img_ping">
      </div>
      <div class="carousel-item">
        <img src="html/img/ex3.jpg" alt="img_ping">
      </div>
      <div class="carousel-item">
        <img src="html/img/ex4.jpg" alt="img_ping">
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#demo" data-slide="prev">
      <i class="fas fa-chevron-left"></i>
    </a>
    <a class="carousel-control-next" href="#demo" data-slide="next">
      <i class="fas fa-chevron-right"></i>
    </a>

  </div>
	</div>
	<br>

	</div>


	
<footer>
	<span>Un site de Samuel LE GALL et Artine ADIKPETO</span>
</footer>
</body>
</html>
