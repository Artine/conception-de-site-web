<?php
session_start();
include('recup.php');	
$_SESSION['erreur']="";
if (isset($_SESSION['connection'])) {
  header ('Location: index_connected.php');
  exit();
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<title>Site projet</title>
  <link rel="stylesheet" href="html/bootstrap-4.3.1/css/bootstrap.css" />
  <link rel="stylesheet" href="html/fontawesome-5.11.2/css/all.css">
  <link rel="stylesheet" href="html/css/style.css">

  <script src="html/js/jquery-2.1.4.min.js"></script>
  <script src="html/js/code_page.js" ></script>
</head>
<body>
<header class="main_header">
	<a href="index.php"><img src="html/img/logo.jpg" alt="logo esigelec"></a>
	<h1>Projet Ping</h1>
	
	<div class="groupement_btns">
	</div>


</header>
<!--
<nav class="main_nav">
	futur navbar
</nav>
-->
<div class="contenu">
	<h3 class="center">Formulaire d'inscription</h3>
  <br>
  <?php 
    if ($_SESSION['erreur_inscr'] == "er_champs"){
      echo '<div class="center"><div class="alert alert-danger" role="alert">
        <span class="fa fa-exclamation-triangle" aria-hidden="true"></span>
        <span class="apres_gly"> Veuillez remplir tous les champs. </span>
      </div></div>'; 
    }else if ($_SESSION['erreur_inscr'] == "er_loginpris"){
      echo '<div class="center"><div class="alert alert-danger" role="alert">
        <span class="fa fa-exclamation-triangle" aria-hidden="true"></span>
        <span class="apres_gly"> Identifiant déjà utilisé. </span>
      </div></div>';
    }else{
    }  
  ?>
<form name="inscription" method="post" action="inscription_page.php">
<div class="center">
  <div class="input-group form-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="fas fa-user"></i></span>
            </div>
            <input name="identifiant" type="text" class="form-control" id="pseudo"  placeholder="taper votre identifiant">
            
  </div></div>
<div class="center">
  <div class="input-group form-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="fas fa-at"></i></span>
            </div>
            <input name="mail" type="email" class="form-control" id="mail" placeholder="taper votre email">       
  </div></div>
  <div class="center">
  <div class="input-group form-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="fas fa-key"></i></span>
            </div>
            <input name="mdp" type="password" class="form-control" id="motdepasse" placeholder="entrer votre mot de passe">       
  </div></div>
  <div class="center">
  <input name="inscrire" type="submit" class="btn btn-primary" value="S'inscrire" />
  </div>
</form>
<br>
<a href="connexion_page.php">Vous êtes déjà inscrit ?</a>

</div>
<footer>
	<span>Un site de Samuel LE GALL et Artine ADIKPETO</span>
</footer>
</body>
</html>