<?php
session_start();
	include('recup.php');
	 if (!isset($_SESSION['connection'])) {
	header ('Location: index.php');
	exit();
}
if (isset($_GET['variable'])) {
	

$_SESSION['vote'] = $_GET['variable'];
setvote();
}
if (isset($_SESSION['erreur_upload'])) {
	$_SESSION['erreur_upload']="";
}

?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<title>Site projet</title>
	<link rel="stylesheet" href="html/bootstrap-4.3.1/css/bootstrap.css" />
	<link rel="stylesheet" href="html/fontawesome-5.11.2/css/all.css">
	<link rel="stylesheet" href="html/css/style.css">

	<script src="html/js/jquery-2.1.4.min.js"></script>
	<script src="html/js/code_page.js" ></script>

</head>
<body>
<header class="main_header">
	<a href="#nogo"><img src="html/img/logo.jpg" alt="logo esigelec"></a>
	<h1>Projet Ping</h1>
	
	<div class="groupement_btns">
		
	<form method="post" action="index.php">
		<input name="deconnecter" type="submit" class="btn btn-primary" value="Se Déconnecter" />
	</form>
	</div>


</header>

<nav>
<ul class="main_nav">
		<li class="nav-item">
			<a class="nav-link home " href="#nogo">
				<span class="fa fa-home" aria-hidden="true"></span>
			</a>
		</li>
		<li class="nav-item ">
			<a class="nav-link " href="liste_election.php">Election(s) terminée(s)</a>
		</li>
		
		
		<?php
	if($_SESSION['admin']==true){
		echo '<li class="nav-item ">
			<a class="nav-link" href="menu_election.php">Menu de gestion des élections</a>
		</li>';

	}

	?>
</ul>
</nav>


<div class="contenu">
	<?php 

			$tab = get_nom_election();
			$num_rows=$tab[0];
			$name_election=$tab[1];
			$liste_id=$tab[2];
			$liste_nom=$tab[3];
			$liste_url=$tab[4];
			$vote=$tab[5];
			$name=$_SESSION['login'];
			echo '<h1>'.$name_election.'</h1>
			<div class="center" >
				<h2>Sujets pings en compétition :</h2>
			</div>
			<hr>';
			if ($vote != null) {
				echo '<div class="center" ><h6>Bonjour '.$name.', vous avez voté pour le sujet n°'.$vote.' <i class="fas fa-question-circle" onclick="visibility(\'non_visible\');"></i></h6> </div><div id="non_visible" >Pour choisir ou modifier votre sujet préféré, cliquer sur l\'image correspondante.</div><hr>';
			}else{
				echo '<div class="center" ><h6>Bonjour '.$name.', vous n\'avez pas encore voté pour votre sujet ping préféré. <i class="fas fa-question-circle" onclick="visibility(\'non_visible\');"></i></h6> </div><div id="non_visible" >Pour choisir ou modifier votre sujet préféré, cliquer sur l\'image correspondante.</div><hr>';
			}

			
	for($i=0;$i<$num_rows;$i++){
			echo '<div class="notif">
			<a href="index_connected.php?variable='.$liste_id[$i].' ">
			<div class="image"><img src="html/img/'.$liste_url[$i].'" alt="img_sujet"></div>
			<div class="nom"> Sujet n°'.($liste_id[$i]).' : '.$liste_nom[$i].' </div></a></div>';
		}
	if($num_rows == 0){
		echo '<div class="center"><div class=" alert alert-danger" role="alert">
				<span class="fa fa-exclamation-triangle" aria-hidden="true"></span>
				<span class="apres_gly">Erreur : Aucune élection n\'est en cours pour le moment  ou aucun sujet ping n\'est ajouté à l\'election en cours.</span>
			</div></div>'; 
	}

	?>


</div>
<footer>
	<span>Un site de Samuel LE GALL et Artine ADIKPETO</span>
</footer>
</body>
</html>
