<?php
session_start();
include('recup.php');
include("upload.php");
if (!isset($_SESSION['connection'])) {
	header ('Location: index.php');
	exit();
}
if ($_SESSION['admin'] == false) {
	header ('Location: index_connected.php');
	exit();
}
if (!isset($_SESSION['erreur_upload'])) {
	$_SESSION['erreur_upload']="";
}


?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<title>Site projet</title>
	<link rel="stylesheet" href="html/bootstrap-4.3.1/css/bootstrap.css" />
	<link rel="stylesheet" href="html/fontawesome-5.11.2/css/all.css">
	<link rel="stylesheet" href="html/css/style.css">

	<script src="html/js/jquery-2.1.4.min.js"></script>
	<script src="html/js/code_page.js" ></script>

</head>
<body>
	<header class="main_header">
		<a href="index_connected.php"><img src="html/img/logo.jpg" alt="logo esigelec"></a>
		<h1>Projet Ping</h1>

		<div class="groupement_btns">

			<form method="post" action="index.php">
				<input name="deconnecter" type="submit" class="btn btn-primary" value="Se déconnecter" />
			</form>
		</div>


	</header>

<nav>
<ul class="main_nav">
		<li class="nav-item">
			<a class="nav-link home " href="index_connected.php">
				<span class="fa fa-home" aria-hidden="true"></span>
			</a>
		</li>
		<li class="nav-item ">
			<a class="nav-link " href="liste_election.php">Election(s) terminée(s)</a>
		</li>
		
		
		<?php
	if($_SESSION['admin']==true){
		echo '<li class="nav-item ">
			<a class="nav-link active" href="menu_election.php">Menu de gestion des élections</a>
		</li>';

	}

	?>
</ul>
</nav>

	<div class="contenu_connected">

    <!-- Debut du formulaire -->

	<?php
	if($_SESSION['erreur_upload'] == 'ok'){
		echo '<div class="center"><div class=" alert alert-success" role="alert">
				<span class="fa fa-check-circle" aria-hidden="true"></span>
				<span class="apres_gly">Upload réussi.</span>
			</div></div>'; 
	}else if($_SESSION['erreur_upload'] == 'er_tel'){
		echo '<div class="center"><div class=" alert alert-danger" role="alert">
				<span class="fa fa-exclamation-triangle" aria-hidden="true"></span>
				<span class="apres_gly">Erreur lors de l\'upload du fichier, veuillez réessayer.</span>
			</div></div>'; 
	}else if($_SESSION['erreur_upload'] == 'er_inc'){
echo '<div class="center"><div class=" alert alert-danger" role="alert">
				<span class="fa fa-exclamation-triangle" aria-hidden="true"></span>
				<span class="apres_gly">Erreur: veuillez choisir <b>UNE IMAGE</b> et donner un nom  au sujet.</span>
			</div></div>'; 
	}else{

	}
    


    ?>
   <form action="ajouter_sujet.php" method="post" enctype="multipart/form-data">
        <h2>Ajout d'un poster</h2>

		<div class="center">
	<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-user"></i></span>
						</div>
						<input name="titre_sujet" type="text" class="form-control" id="pseudo"  placeholder="taper le nom du projet">
						
					</div>
</div>

        <label for="fileUpload">Fichier:</label>
        <input type="file" name="photo" id="fileUpload" accept=".jpg, .jpeg, .png">
        <input type="submit" name="submit" value="Envoyer l'image">
        <p><strong>Note:</strong> Seuls les formats .jpg, .jpeg, .gif, .png sont autorisés jusqu'à une taille maximale de 2 Mo.</p>

    </form>



	</div>

	<footer>
		<span>Un site de Samuel LE GALL et Artine ADIKPETO</span>
	</footer>
</body>
</html>
