<?php
session_start();
	include('recup.php');
	 if (!isset($_SESSION['connection'])) {
	header ('Location: index.php');
	exit();
}
if ($_SESSION['admin'] == false) {
	header ('Location: index_connected.php');
	exit();
}
if (!isset($_SESSION['erreur_crea_election'])) {
	$_SESSION['erreur_crea_election']="";
}

?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<title>Site projet</title>
	<link rel="stylesheet" href="html/bootstrap-4.3.1/css/bootstrap.css" />
	<link rel="stylesheet" href="html/fontawesome-5.11.2/css/all.css">
	<link rel="stylesheet" href="html/css/style.css">

	<script src="html/js/jquery-2.1.4.min.js"></script>
	<script src="html/js/code_page.js" ></script>

</head>
<body>
<header class="main_header">
	<a href="index_connected.php"><img src="html/img/logo.jpg" alt="logo esigelec"></a>
	<h1>Projet Ping</h1>
	
	<div class="groupement_btns">
		
	<form method="post" action="index.php">
		<input name="deconnecter" type="submit" class="btn btn-primary" value="Se déconnecter" />
	</form>
	</div>


</header>

<nav>
<ul class="main_nav">
		<li class="nav-item">
			<a class="nav-link home " href="index_connected.php">
				<span class="fa fa-home" aria-hidden="true"></span>
			</a>
		</li>
		<li class="nav-item ">
			<a class="nav-link " href="liste_election.php">Election(s) terminée(s)</a>
		</li>
		
		
		<?php
	if($_SESSION['admin']==true){
		echo '<li class="nav-item ">
			<a class="nav-link" href="menu_election.php">Menu de gestion des élections</a>
		</li>';

	}

	?>
</ul>
</nav>

<div class="contenu">
	<h2>Nom de l'élection</h2>
	<br>
<form id="corps" method="post" action="Creation.php">
              

              <?php 
    if ($_SESSION['erreur_crea_election'] == "er_champs"){
      echo '<div class="center"><div class="alert alert-danger" role="alert">
        <span class="fa fa-exclamation-triangle" aria-hidden="true"></span>
        <span class="apres_gly"> veuillez remplir tous les champs </span>
      </div></div>'; 
    }else{
    }  
  ?>
			<div class="center">
  <div class="input-group form-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="fas fa-file-signature"></i></span>
            </div>
            <input type="text" name="nom_election"  class="form-control" placeholder="Veuillez entrer le nom de l'élection">      
  </div></div>
              <input name="nom_election_ok" type="submit" class="btn btn-primary" value="Créer l'élection" />
          </form>

</div>
<footer>
	<span>Un site de Samuel LE GALL et Artine ADIKPETO</span>
</footer>
</body>
</html>
