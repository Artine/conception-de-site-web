<?php
session_start();
include('recup.php');
if (!isset($_SESSION['connection'])) {
	header ('Location: index.php');
	exit();
}
if (isset($_SESSION['erreur_upload'])) {
	$_SESSION['erreur_upload']="";
}


?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<title>Site projet</title>
	<link rel="stylesheet" href="html/bootstrap-4.3.1/css/bootstrap.css" />
	<link rel="stylesheet" href="html/fontawesome-5.11.2/css/all.css">
	<link rel="stylesheet" href="html/css/style.css">

	<script src="html/js/jquery-2.1.4.min.js"></script>
	<script src="html/js/code_page.js" ></script>
</head>
<body>
	<header class="main_header">
		<a href="index_connected.php"><img src="html/img/logo.jpg" alt="logo esigelec"></a>
		<h1>Projet Ping</h1>

		<div class="groupement_btns">

			<form method="post" action="index.php">
				<input name="deconnecter" type="submit" class="btn btn-primary" value="Se déconnecter" />
			</form>
		</div>


	</header>

<nav>
<ul class="main_nav">
		<li class="nav-item">
			<a class="nav-link home " href="index_connected.php">
				<span class="fa fa-home" aria-hidden="true"></span>
			</a>
		</li>
		<li class="nav-item ">
			<a class="nav-link active" href="#nogo">Election(s) terminée(s)</a>
		</li>
		
		
		<?php
	if($_SESSION['admin']==true){
		echo '<li class="nav-item ">
			<a class="nav-link" href="menu_election.php">Menu de gestion des élections</a>
		</li>';

	}

	?>
</ul>
</nav>

	<div class="contenu_connected">
		<?php 
		
		$tab = get_liste_election_closed();
		$num_rows=$tab[0];
		$liste_id=$tab[1];
		$name_election=$tab[2];

		for($i=0;$i<$num_rows;$i++){
			echo '<div class="lien_election"><a href="election.php?num_election='.$liste_id[$i].'"><i class="fas fa-arrow-circle-right"></i> '.$name_election[$i].'</a></div>';
		}

		

		?>

	</div>

	<footer>
		<span>Un site de Samuel LE GALL et Artine ADIKPETO</span>
	</footer>
</body>
</html>
